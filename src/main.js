import 'bootstrap/dist/css/bootstrap.css'
import './assets/main.css'

import { createApp } from 'vue'
import PrimeVue from 'primevue/config'
import InputText from 'primevue/inputtext'
import FloatLabel from 'primevue/floatlabel'
import Button from 'primevue/button'

import Sidebar from 'primevue/sidebar'

import App from './App.vue'
import router from './router'
import 'primevue/resources/themes/lara-light-indigo/theme.css'
import 'primevue/resources/primevue.min.css'
import 'primeicons/primeicons.css'

const app = createApp(App)

app.use(router)
app.use(PrimeVue)

// eslint-disable-next-line vue/multi-word-component-names, vue/no-reserved-component-names
app.component('Button', Button)
app.component('InputText', InputText)
app.component('FloatLabel', FloatLabel)
// eslint-disable-next-line vue/multi-word-component-names
app.component('Sidebar', Sidebar)

app.mount('#app')

import 'bootstrap/dist/js/bootstrap.js'
