import { createRouter, createWebHistory } from 'vue-router'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      component: () => import('../views/GerenteView.vue')
    },
    {
      path: '/asesor',
      name: 'asesor',
      component: () => import('../views/AsesorView.vue')
    },
    {
      path: '/coordinador',
      name: 'coordinador',
      component: () => import('../views/CoordinadorView.vue')
    },
    {
      path: '/supervisor',
      name: 'supervisor',
      component: () => import('../views/SupervisorView.vue')
    }
  ]
})

export default router
